import { Request ,Response, NextFunction } from "express";
import Joi from "joi";

const schema = Joi.object({
    first_name: Joi.string()
        .pattern(new RegExp(/[a-zA-Z]+/))
        .min(3)
        .max(30)
        .required(),
    last_name: Joi.string()
    .pattern(new RegExp(/[a-zA-Z]+/))
    .min(3)
    .max(30)
    .required(),
    songs : Joi.array(),
    status : Joi.string().required()
})
export async function checkArtist(req:Request,res:Response,next:NextFunction){
    try{
       await schema.validateAsync(req.body);
        next();
    }
    catch(err){
        console.log("catch");
        next(err);
    }
}


/* eslint-disable prefer-const */
/* eslint-disable @typescript-eslint/no-explicit-any */
  import * as DB_ARTIST from "../../db/artist.db.js"
import { Artist } from "./artist.interface.js";

  export async function createArtist(artist:Artist){
    return await DB_ARTIST.createArtist(artist);
   }
   export async function getAllArtists(){
    return await DB_ARTIST.getAllArtists();
   }
export async function getArtistByid(id:string) {
    return await DB_ARTIST.getArtistByid(id);
}

export async function updateArtist(id:string, artist:Partial<Artist>) { 
  return await DB_ARTIST.updateArtist(id,artist);
}
export async function updateStatus_artist(id:string, status:string) { 
  return await DB_ARTIST.updateStatus_artist(id,status);
}


export async function deleteArtist(id:string){
  return await DB_ARTIST.deleteArtist(id);
}






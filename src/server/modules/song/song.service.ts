/* eslint-disable prefer-const */
/* eslint-disable @typescript-eslint/no-explicit-any */
import * as DB_SONG from "../../db/song.db.js"
import { Song } from "./song.interface.js";

// post - /api/songs/
export const create_song = async (payload: Song,artist_id:string) => {
    return await DB_SONG.create_song(payload,artist_id);
}

export const updateStatus_song = async (song_id:string,status:string) => {
    return await DB_SONG.updateStatus_song(song_id,status);
}
//get - /api/song/
export const get_all_songs = async () => {
    return await DB_SONG.get_all_songs();
}
// get - /api/song/user/:id
export const get_artist_song = async (artist_id:string) => {
    return await DB_SONG.get_artist_song(artist_id);
}

 // get - /api/song/:id
export const getSongById = async (song_id:string) => {
    return await DB_SONG.getSongById(song_id);
}

// // put - /api/song/:id
export const updateSongById = async (song_id:string, payload:Partial<Song>) => {
    return await DB_SONG.updateSongById(song_id,payload);
}

// delete - /api/song/:id
export const deleteSongById = async (song_id:string) => {
    return await DB_SONG.deleteSongById(song_id);
}
import { Request ,Response, NextFunction } from "express";
import Joi from "joi";

const schema = Joi.object({
   name: Joi.string()
        .pattern(new RegExp(/[a-zA-Z]+/))
        .min(3)
        .max(30)
        .required(),
    url: Joi.string()
    .required(),
    artist: Joi.number().required(),
    status : Joi.string()
})
export async function checkSong(req:Request,res:Response,next:NextFunction){
    try{
       await schema.validateAsync(req.body);
        next();
    }
    catch(err){
        console.log("catch");
        next(err);
    }
}

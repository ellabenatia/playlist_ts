/* eslint-disable @typescript-eslint/no-explicit-any */
import express, { Response, Request} from "express"
// import log from '@ajar/marker'
import raw from "../../middleware/route.async.wrapper.js"
import * as song_service from "./song.service.js"
import {checkSong} from "./song.validator.js";
import { verifyAuth } from "../../middleware/auth_func.js";
import { verifyAuto } from "../../middleware/autorization_func.js";
const router = express.Router()
router.use(express.json())

router.route("/") 
    .post(verifyAuth,checkSong,raw(async (req:Request, res:Response) => { // CREATES A NEW SONG
      const song = await song_service.create_song(req.body, req.body.artist);
      res.status(200).json(song);
    }))
    .get(raw(async ( _:any , res:Response) => { // GET ALL SONGS
      const songs = await song_service.get_all_songs();
      res.status(200).json(songs);
    }))

router.get("/artist/:id", raw( async (req, res) => { // GET ALL SONGS OF A ARTIST
  const user = await song_service.get_artist_song(req.params.id);
  res.status(200).json(user);
})) 
router.get("/updateStatus/:status/:id",verifyAuth, verifyAuto("MODERATOR,ADMIN"), raw( async (req, res) => { // update_status
  const status = await song_service.updateStatus_song(req.params.id,req.params.status);
  res.status(200).json(status);
})) 
// export async function updateStatus_song(req: Request, res: Response, next: NextFunction){


 router.route("/:id") 
    .get(raw(async (req, res) => { // GETS SONG BY ID
      const song = await song_service.getSongById(req.params.id)
      if (!song) return res.status(404).json({ status: "No song found." });
      res.status(200).json(song);
    })) 
    .post(verifyAuth, verifyAuto("MODERATOR,ADMIN"),raw(async (req, res) => { // UPDATE SONG BY ID 
      const update_song = await song_service.updateSongById(req.params.id,req.body);
      res.status(200).json(update_song);
    }))
    .delete(verifyAuth,verifyAuto("MODERATOR,ADMIN"),raw(async (req, res) => { // DELETE SONG BY ID
      const delete_song = await song_service.deleteSongById(req.params.id);
      if (!delete_song) return res.status(404).json({ status: "No story found." });
      res.status(200).json(delete_song);
    }))

export default router;
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prefer-const */
import  { Response, Request} from "express"
import {createAcsessToken, createRefreshToken} from "./auth.tokenGen.js";
import bcrypt from "bcryptjs";
import ms from 'ms';
import jwt from 'jsonwebtoken';
import {getUserByEmail}from "../../db/user.db.js";

import {create_user,updateUserById}  from "../user/user.service.js";
const {APP_SECRET = "supercalifragilisticexpialidocious"} = process.env;


export async function auth_login(req:Request,res:Response){
    const {email,password} = req.body;
    let testUser = await getUserByEmail(email);
    const auth = bcrypt.compare(testUser.password,password);
    if(!auth){
      return null;
    }
        // create a token
        const acessToken = createAcsessToken( testUser.id,testUser.roles,'acessToken');
        const refreshToken = createRefreshToken( testUser.id,testUser.roles,'refreshToken');
        await updateUserById(testUser.id,{refreshToken:refreshToken});
        res.cookie('refreshToken',refreshToken, {
            maxAge: ms('60d'), //60 days
            httpOnly: true
          });
        const payload = {...testUser};
        delete payload.password;
        res.status(200).json({
            status:'you are authenticated', 
            testUser,
            acessToken : acessToken,
            refreshToken : refreshToken
        })
}

export async function get_acess_token(req:Request, res:Response){
    //get refresh_token from client - req.cookies
    const {refreshToken} = req.cookies;
    
    if (!refreshToken) return res.status(403).json({
        status:'Unauthorized',
        payload: 'No refresh_token provided.'
    });
      // verifies secret and checks expiration
      const decoded = jwt.verify(refreshToken, APP_SECRET);
    
      // casting need to fix
    const {id,roles, some} = decoded as jwt.JwtPayload;
    const acessToken = createAcsessToken(id,roles,some);
      res.status(200).json({acessToken})  
  }
  export async function auth_logout(req:Request, res:Response){
    const {id} = req.body;
    await updateUserById(id,{refreshToken:""});
    //  await userModel.findOneAndUpdate(id,{refreshToken:""});
     res.clearCookie('refreshToken');
     res.status(200).json({status:'You are logged out'})
  }
  export async function auth_register(req:Request, res:Response){ // CREATES A NEW USER
    const user = await create_user(req.body);
    res.status(200).json(user);
  }

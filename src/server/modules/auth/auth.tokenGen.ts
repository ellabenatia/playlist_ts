import jwt from 'jsonwebtoken';

const {ACSESS_TOKEN_EXPIRATION, REFRESH_TOKEN_EXPIRATION ,APP_SECRET = "supercalifragilisticexpialidocious"} = process.env;

export function createAcsessToken (userId:string, roles:string, some:string){
return jwt.sign({ id : userId ,roles:roles, some: some}, APP_SECRET, {
    expiresIn: ACSESS_TOKEN_EXPIRATION // expires in 1 minute for debugging...
})
}

export function createRefreshToken (userId:string,roles:string, some:string){
    return jwt.sign({ id : userId ,roles:roles, some:some}, APP_SECRET , {
        expiresIn: REFRESH_TOKEN_EXPIRATION // expires in 1 minute for debugging...
    })
}

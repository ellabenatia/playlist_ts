/* eslint-disable prefer-const */
/* eslint-disable @typescript-eslint/no-explicit-any */
import express from "express"
// import log from '@ajar/marker'
import raw from "../../middleware/route.async.wrapper.js"



import cookieParser from 'cookie-parser';
import { auth_login,get_acess_token,auth_logout, auth_register } from "./auth.service.js";


const router = express.Router()
router.use(express.json())
router.use(cookieParser());

router.post('/login', raw(auth_login));
router.get('/get-access-token',raw(get_acess_token));
router.get('/logout', raw(auth_logout));
router.post('/register',raw(auth_register));


export default router;
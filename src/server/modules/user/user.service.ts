/* eslint-disable prefer-const */
/* eslint-disable @typescript-eslint/no-explicit-any */
import * as DB_USER from "../../db/user.db.js"
import { User } from "./user.interface.js";
 
// post - /api/songs/
export const create_user = async (payload: User) => {
    return await DB_USER.create_user(payload);
}
//get - /api/song/
export const getAllUsers = async () => {
    return await DB_USER.getAllUsers();
}
//get - /api/user/playlists/:id
export const getPlaylistsUser = async (user_id:string) => {
    return await DB_USER.getPlaylistsUser(user_id);
}

 // get - /api/song/:id
export const getUserById = async (user_id:string) => {
    return await DB_USER.getUserById(user_id);
}

// // put - /api/song/:id
export const updateUserById = async (user_id:string, payload:Partial<User>) => {
    return await DB_USER.updateUser(user_id,payload);
}

// delete - /api/song/:id
export const deleteUserById = async (user_id:string) => {
    return await DB_USER.deleteUserById(user_id);
}
/* eslint-disable @typescript-eslint/no-explicit-any */
import express, { Response, Request} from "express"
// import log from '@ajar/marker'
import raw from "../../middleware/route.async.wrapper.js"
import * as user_service from "./user.service.js"
import {checkUser} from "./user.validator.js";
import {verifyAuth} from "../../middleware/auth_func.js"
import { verifyAuto } from "../../middleware/autorization_func.js";


const router = express.Router()
router.use(express.json())

router.route("/") 
    .post(checkUser,raw(async (req:Request, res:Response) => { // CREATES A NEW USER
      const user = await user_service.create_user(req.body);
      res.status(200).json(user);
    }))
    .get(verifyAuth,verifyAuto("ADMIN"),raw(async ( _:any , res:Response) => { // GET ALL USERS
      const stories = await user_service.getAllUsers();
      res.status(200).json(stories);
    }))

router.get("/playlists/:id", raw( async (req, res) => { // GET ALL PLAYLISTS OF A USER
  const user = await user_service.getPlaylistsUser(req.params.id);
  res.status(200).json(user);
})) 


router.route("/:id") 
    .get(raw(async (req, res) => { // GETS USER BY ID
      const song = await user_service.getUserById(req.params.id)
      if (!song) return res.status(404).json({ status: "No song found." });
      res.status(200).json(song);
    })) 
    .post(raw(async (req, res) => { // UPDATE USER BY ID 
      const update_song = await user_service.updateUserById(req.params.id,req.body);
      res.status(200).json(update_song);
    }))
    .delete(raw(async (req, res) => { // DELETE USER BY ID
      const delete_user = await user_service.deleteUserById(req.params.id);
      if (!delete_user) return res.status(404).json({ status: "No story found." });
      res.status(200).json(delete_user);
    }))

export default router;
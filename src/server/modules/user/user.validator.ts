import { Request ,Response, NextFunction } from "express";
import Joi from "joi";

const schema = Joi.object({
   first_name: Joi.string()
        .pattern(new RegExp(/[a-zA-Z]+/))
        .min(3)
        .max(30)
        .required(),
    last_name: Joi.string()
        .pattern(new RegExp(/[a-zA-Z]+/))
        .min(3)
        .max(30)
        .required(),
    playlists : Joi.array(),
    email: Joi.string(),
    password :  Joi.string(),
    refreshToken : Joi.string().allow(null),
    roles: Joi.string().required()
})
export async function checkUser(req:Request,res:Response,next:NextFunction){
    try{
       await schema.validateAsync(req.body);
        next();
    }
    catch(err){
        console.log("catch");
        next(err);
    }
}

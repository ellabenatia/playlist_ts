/* eslint-disable @typescript-eslint/no-explicit-any */
import express, { Response, Request} from "express"
// import log from '@ajar/marker'
import raw from "../../middleware/route.async.wrapper.js"
import * as playlist_service from "./playlist.service.js"
import {checkPlaylist} from "./playlist.validator.js";


const router = express.Router()
router.use(express.json())

router.route("/") 
    .post(checkPlaylist, raw(async (req:Request, res:Response) => { // CREATES A NEW PLAYLIST
      const song = await playlist_service.create_playlist(req.body,req.body.createBy);
      res.status(200).json(song);
    }))
    .get(raw(async ( _:any , res:Response) => { // GET ALL PLAYLIST
      const stories = await playlist_service.getAllPlaylists();
      res.status(200).json(stories);
    }))

    router.post("/removeSong", raw( async (req, res) => { 
      const user = await playlist_service.removeSongFromPlaylist(req.body.playlist);
      res.status(200).json(user);
    })) 

    router.post("/addSong", raw( async (req, res) => { 
      const user = await playlist_service.addSongToPlaylist(req.body.playlist, req.body.song);
      res.status(200).json(user);
    })) 
      router.route("/:id") 
          .get(raw(async (req, res) => { // GETS PLAYLIST BY ID
            const playlist = await playlist_service.getPlaylistById(req.params.id)
            if (!playlist) return res.status(404).json({ status: "No playlist found." });
            res.status(200).json(playlist);
          })) 
          .post(raw(async (req, res) => { // UPDATE PLAYLIST BY ID 
            const update_playlist = await playlist_service.updatePlaylistById(req.params.id,req.body);
            res.status(200).json(update_playlist);
          }))
          .delete(raw(async (req, res) => { // DELETE PLAYLIST BY ID
            const delete_playlist = await playlist_service.deletePlaylistById(req.params.id);
            if (!delete_playlist) return res.status(404).json({ status: "No playlist found." });
            res.status(200).json(delete_playlist);
          }))

export default router;
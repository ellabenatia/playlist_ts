/* eslint-disable prefer-const */
/* eslint-disable @typescript-eslint/no-explicit-any */

import * as DB_PLAYLIST from "../../db/playlist.db.js"
import { Playlist } from "./playlist.interface.js";

// post - /api/playlist/
export const create_playlist = async (payload: Playlist,user_id:string) => {
    return await DB_PLAYLIST.create_playlist(payload,user_id);
}

export const addSongToPlaylist = async (playlist_id:string, song_id:string) => {
    return await DB_PLAYLIST.addToPlaylist_Song(playlist_id,song_id);
}


export const getAllPlaylists = async () => {
    return await DB_PLAYLIST.getAllPlaylists();
   }

   export const removeSongFromPlaylist = async (playlist_id:string) => {
    return await DB_PLAYLIST.deleteFromPlaylist_Song(playlist_id);
}
   
// //help to delete palylist by id
//    export const removePlaylistFromSong = async (playlist_id:string,song_id:string) => {
//     await DB_PLAYLIST.removePlaylistFromSong(playlist_id, song_id);
// }
// export const removeSongFromPlaylist = async (playlist_id:string,song_id:string) => {
//     return await DB_PLAYLIST.removeSongFromPlaylist(playlist_id,song_id);
// }

export const getPlaylistById = async (playlist_id:string) => {
    return await DB_PLAYLIST.getPlaylistById(playlist_id);
}

// // put - /api/stories/:id
export const updatePlaylistById = async (playlist_id:string, payload:Partial<Playlist>) => {
    return await DB_PLAYLIST.updatePlaylistById(playlist_id, payload);
}

// delete - /api/stories/:id
export const deletePlaylistById = async (playlist_id:string) => {
    return await DB_PLAYLIST.deletePlaylistById(playlist_id);
}
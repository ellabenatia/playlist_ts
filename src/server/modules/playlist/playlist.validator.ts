import { Request ,Response, NextFunction } from "express";
import Joi from "joi";

const schema = Joi.object({
    playlistName: Joi.string()
        .pattern(new RegExp(/[a-zA-Z]+/))
        .min(3)
        .max(30)
        .required(),
    createBy:Joi.number()
})
export async function checkPlaylist(req:Request,res:Response,next:NextFunction){
    try{
       await schema.validateAsync(req.body);
        next();
    }
    catch(err){
        console.log("catch");
        next(err);
    }
}

import { Request, Response, NextFunction} from "express";
import jwt from 'jsonwebtoken';

const {APP_SECRET = "supercalifragilisticexpialidocious"} = process.env;
export const verifyAuth = async (req:Request, res:Response, next:NextFunction) => {
        // check header or url parameters or post parameters for token
        const accessToken  = req.headers['x-access-token'] as string | undefined;
        if (!accessToken) return res.status(403).json({
            status:'Unauthorized',
            payload: 'No token provided.'
        });
        // verifies secret and checks exp
        try{
           //decoded obj
           const decoded_obj =jwt.verify(accessToken, APP_SECRET) as jwt.JwtPayload;
           req.id = decoded_obj.id;
           req.roles = decoded_obj.roles;
           next();
        }
     catch(error){
        next(error);
     }
       
  }

//   export const verifyAuterization = (roles_arr)=>{
//    const errorsFileLogger = fs.createWriteStream('./src/server/log/error.log', { flags: 'a' });
//    return (req: Request, res: Response, next: NextFunction) => {
//        errorsFileLogger.write(`${req.id} : ${err.message} >> ${err.stack} \n`);
//        next(err);
//    }
// }
import { NextFunction, Request, Response } from "express";

const uid = () => Math.random().toString(36).substring(6);

export function genId (req: Request, res: Response, next: NextFunction) {
    req.id = uid();
    next();
};
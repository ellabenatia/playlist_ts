/* eslint-disable @typescript-eslint/no-inferrable-types */
import { NextFunction, Request, Response } from 'express';
import fs from 'fs/promises';

export async function user_log(req:Request,res:Response,next:NextFunction){
    const time: number = Date.now();
    const type = req.method;
    const path = req.url;
    const new_log:string = `${type} ${path} / ${time} \n`;
    await fs.appendFile('./src/server/log/http.log',new_log);
    console.log();
    next();
}
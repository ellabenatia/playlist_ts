/* eslint-disable prefer-const */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Request, Response, NextFunction} from "express";

export function verifyAuto(roles_accepted:string){
const roles_accepted_arr= roles_accepted.split(",");
return  async function(req: Request, res: Response, next: NextFunction) {
    let {id,roles} = req;
    let id_roles_arr = roles.split(",");
    let found = id_roles_arr.some(id_role=> roles_accepted_arr.includes(id_role));
    if(!found){
        console.log(id_roles_arr);
        return res.status(403).json({
            status:'you dont have permmission for this action',
            payload: 'roles isnt accept'
        });
    }
    next();
}}

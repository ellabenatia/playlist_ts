// require('dotenv').config();
import express from "express";
import { genId } from "./middleware/utils.js";
import morgan from "morgan";
// import log from "marker";
import cors from "cors";

import { connect } from "./db/mysql.connection.js";
import artist_router from "./modules/artist/artist.router.js";
import song_router from "./modules/song/song.router.js";
import playlist_router from "./modules/playlist/playlist.router.js";
import user_router from "./modules/user/user.router.js";
import auth_router from "./modules/auth/auth.router.js";
import {job} from "./db/sceduler.js"; 



import {
    error_handler,
    error_handler2,
    logError,
    not_found,
} from "./middleware/errors.handler.js";
import { user_log } from "./middleware/user-func.js";

const {
    PORT = 8080,
    HOST = "localhost",
} = process.env;
class Api {
  
    private app: express.Application;

    constructor() {
        this.app = express();

        this.applyGlobalMiddleware();
        this.routing();
    }
    applyGlobalMiddleware() {
        this.app.use(cors());
        this.app.use(user_log);
        this.app.use(morgan("dev"));
    }
    routing() {
        this.app.use(genId);
        this.app.use("/api/auth", auth_router);
        this.app.use("/api/artist", artist_router);
        this.app.use("/api/songs", song_router);
        this.app.use("/api/playlist", playlist_router);
        this.app.use("/api/user",user_router);

        // central error handling
        this.app.use(error_handler);
        this.app.use(logError());
        this.app.use(error_handler2);
        //when no routes were matched...
        this.app.use("*", not_found);
    }
    //start the express api server
    async startServer(){
        try {
            //connect to mySql
             await connect();
            await this.app.listen(Number(PORT), HOST);
            //backup
           //job.start();
            console.log(
                "api is live on",
                ` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`
            );
        } catch (err) {
            console.log;
        }
    }
}

const api = new Api();
api.startServer();

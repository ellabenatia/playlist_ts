/* eslint-disable prefer-const */
/* eslint-disable @typescript-eslint/no-explicit-any */
import bcrypt from "bcryptjs";
import { OkPacket, RowDataPacket } from "mysql2";
import { connection as db } from "./mysql.connection.js";
import { deleteAllPlaylistOfuser } from "./playlist.db.js";


// post - /api/songs/
export const create_user = async function(payload: any):Promise<[number,{message:string; result:OkPacket}]>{
    const {password} = payload; 
    const hash = await bcrypt.hash(password,8);
    payload.password = hash;
    const sql = `INSERT INTO user SET ?`;
    let results = await db.query(sql,payload);
    const result : OkPacket = results[0] as OkPacket;
    const ok = {status:200, message:"user created succssfuly"};
     const fail = {status:200, message:"user creation failed"};
     const {status, message} = result.affectedRows? ok: fail;
    return [status , {message, result}]; 
}

export async function getAllUsers(){
    const sql_get = `SELECT * FROM user`;
    let [rows] = await db.query(sql_get);
    return rows;
}

export async function updateUser(id:string, user:any) { 
    const sql_update = `UPDATE user SET ? WHERE id=?`;
    let results = await db.query(sql_update,[user,id]);
    const result : RowDataPacket = results[0] as RowDataPacket;
    const ok = {status:200, message:"user update succssfuly"};
     const fail = {status:200, message:"user update failed"};
     const {status, message} = result.affectedRows? ok: fail;
    return [status , {message, result}];
}

 // get - /api/song/:id
export const getUserById = async (user_id:string) => {
    const sql_get = `SELECT * FROM user where id = ${user_id}`;
    let [rows] = await db.query(sql_get) as any;
    return rows[0];
}
export const getUserByEmail = async (user_email:string) => {
    const sql_get = `SELECT * FROM user where email = ?`;
    let [rows] = await db.query(sql_get,user_email) as any;
    return rows[0];
}
export const getPlaylistsUser= async (user_id:string) => {
    const sql_get = `SELECT * FROM playlist where createdBy = ${user_id}`;
    let [rows] = await db.query(sql_get);
    return rows;
}

// delete playlist
export const deleteUserById = async (user_id:string) => {
    await getUserById(user_id);
    await deleteAllPlaylistOfuser(user_id);
    const sql_delete =`DELETE FROM user WHERE id=${user_id}`;
    let [rows] = await db.query(sql_delete);
    return rows;
}
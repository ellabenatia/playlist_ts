/* eslint-disable prefer-const */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { connection as db} from "../db/mysql.connection.js";
import { OkPacket, RowDataPacket } from 'mysql2';


// post - /api/playlist/

export const create_playlist = async (payload: any, user_id:string) => {
    const sql_check =  `SELECT * FROM artist where id = ${user_id}`;
    let rows = await db.query(sql_check) as any;
    if(!rows || !rows[0]){
        throw new Error("no such user exit");
    }
    const sql = `INSERT INTO playlist SET ?`;
    let results = await db.query(sql,payload);
    const result : OkPacket = results[0] as OkPacket;
    const ok = {status:200, message:"playlist created succssfuly"};
     const fail = {status:200, message:"playlist creation failed"};
     const {status, message} = result.affectedRows? ok: fail;
    return [status , {message, result}]; 
}

export const addToPlaylist_Song= async (playlist_id:string, song_id:string) => {
    const sql = `INSERT INTO playlist_song SET ?`;
    let results = await db.query(sql,{playlist_id,song_id});
    const result : OkPacket = results[0] as OkPacket;
    const ok = {status:200, message:"added playlist_song succssfuly"};
     const fail = {status:200, message:"added playlist_song creation failed"};
     const {status, message} = result.affectedRows? ok: fail;
    return [status , {message, result}]; 
}
export const deleteFromPlaylist_Song= async (playlist_id:string) => {
    let sql = `DELETE FROM playlist_song WHERE playlist_id=${playlist_id}`;
    let [rows] = await db.query(sql);
    return rows; 
}
export const getAllPlaylists = async () => {
    const sql_get = `SELECT * FROM playlist`;
    let [rows] = await db.query(sql_get);
    return rows;
   }

export const getPlaylistById = async (playlist_id:string) => {
    const sql_check =  `SELECT * FROM playlist where id = ${playlist_id}`;
    let [rows] = await db.query(sql_check) as any;
    if(!rows || !rows[0]){
        throw new Error("no such user exit");
    }
    return rows;
}

// // put - /api/stories/:id
export const updatePlaylistById = async (playlist_id:string, payload:any) => {
    const sql_update = `UPDATE playlist SET ? WHERE id=?`;
    let results = await db.query(sql_update,[payload,playlist_id]);
    const result : RowDataPacket = results[0] as RowDataPacket;
    const ok = {status:200, message:"playlist update succssfuly"};
     const fail = {status:200, message:"playlist update failed"};
     const {status, message} = result.affectedRows? ok: fail;
    return [status , {message, result}];
}
// delete - /api/stories/:id
export const deletePlaylistById = async (playlist_id:string) => {
    let sql = `DELETE FROM playlist_song WHERE playlist_id=${playlist_id}`;
    await db.query(sql);
    sql = `DELETE FROM playlist WHERE id=${playlist_id}`;
    let [rows] = await db.query(sql);
    return rows;
    
}
export const deleteAllPlaylistOfuser = async(user_id:string)=>{
    const sql_get = `DELETE FROM playlist WHERE createdBy=${user_id}`;
    let [rows] = await db.query(sql_get);
    return rows;
}
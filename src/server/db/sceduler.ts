/* eslint-disable prefer-const */
/* eslint-disable @typescript-eslint/no-unused-vars */
import {CronJob} from "cron";
import { exec } from "child_process";
import fetch from 'node-fetch';

export const job = new CronJob(
  "* * * * *",
  async () => {
    let timeStamp = Date.now().toString();
    const child = exec("docker exec mysql-db /usr/bin/mysqldump -u root --password=qwerty billing")
    await fetch('http://localhost:3031/scheduler', {
    method: 'POST',
    body: child.stdout,
    headers:{"file_name":`${timeStamp}.dump.sql` } 
});
    console.log("backup completed with status code: ");
  },
  null,
  true,
);

// Begin the cronjob
//job.start();
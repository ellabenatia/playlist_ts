/* eslint-disable prefer-const */
/* eslint-disable @typescript-eslint/no-explicit-any */
// import { deleteSongById } from "../song/song.service.js";
import { deleteAllSongsOfArtist } from "../db/song.db.js";
//import song_model from "../song/song.model.js"
//import log from '@ajar/marker'
import { connection as db} from "../db/mysql.connection.js";
import { OkPacket, RowDataPacket } from "mysql2";

export async function createArtist(artist:any):Promise<[number,{message:string; result:OkPacket}]>{
  const sql = `INSERT INTO artist SET ?`;
  let results = await db.query(sql,artist);
  const result : OkPacket = results[0] as OkPacket;
  const ok = {status:200, message:"artist creatred succssfuly"};
   const fail = {status:200, message:"artist creatred succssfuly"};
   const {status, message} = result.affectedRows? ok: fail;
  return [status , {message, result}];
}

   export async function getAllArtists(){
    const sql_get = `SELECT * FROM artist`;
    let [rows] = await db.query(sql_get);
    return rows;
   }
export async function getArtistByid(id:string) {
  const sql_get = `SELECT * FROM artist where id = ${id}`;
    let [rows] = await db.query(sql_get) as any;
    if(!rows || !rows[0]){
      throw new Error("no such artist exit");
  }
    return rows;
}

export async function updateArtist(id:string, payload:any) { 
  const sql_update = `UPDATE artist SET ? WHERE id=?`;
  let results = await db.query(sql_update,[payload,id]);
  const result : RowDataPacket = results[0] as RowDataPacket;
  const ok = {status:200, message:"Artist update succssfuly"};
   const fail = {status:200, message:"Artist update failed"};
   const {status, message} = result.affectedRows? ok: fail;
  return [status , {message, result}];
}
function object_to_array(songs:any) {
  let ans = songs.reduce((arr:any,song:any)=>{
  arr.push(song.id);
  return arr;
  },[]);
  return ans;
}
export const updateStatus_artist = async (artist_id:string,status:string) => {
  const sql_get = `SELECT * FROM statuses where status_name=?`;
  let [rows] = await db.query(sql_get,status) as any;
  if(!rows || !rows[0]){
      throw new Error("unexcepted status!");
  }
  const sql_update = `UPDATE artist SET status = ? WHERE id=?`;
  let [result] = await db.execute(sql_update,[status,artist_id]);
  return result;
}

export async function deleteArtist(id:string){
  const songs_query = "SELECT id FROM song WHERE artist=?";
  let params=[id];
  let [songs,] =  await db.execute(songs_query,params);
  const songs_to_delete= object_to_array(songs);
  // delete from playlist_songf_of artist
  const query = `DELETE FROM playlist_song WHERE song_id IN(${songs_to_delete})`;
  await db.execute(query);
  let value  = await getArtistByid(id);
  if(!value){
    throw new Error("artist not exist");
  }
  await deleteAllSongsOfArtist(id);
  const sql_delete =`DELETE FROM artist WHERE id=${id}`;
  let [rows] = await db.query(sql_delete);
  return rows;
}



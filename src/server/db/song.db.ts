/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prefer-const */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { OkPacket, RowDataPacket } from "mysql2";
import { connection as db} from "../db/mysql.connection.js";

// post - /api/songs/
export const create_song = async function (payload: any, artist_id:string){
    const sql_check =  `SELECT * FROM artist where id = ${artist_id}`;
    let rows = await db.query(sql_check) as any;
    if(!rows || !rows[0]){
        throw new Error("no such artist exit");
    }
    const sql_insert = `INSERT INTO song SET ?`;
    let results = await db.query(sql_insert,payload);
    const result : OkPacket = results[0] as OkPacket;
    const ok = {status:200, message:"song creatred succssfuly"};
     const fail = {status:200, message:"song creatred succssfuly"};
     const {status, message} = result.affectedRows? ok: fail;
    return [status , {message, result}];
}

//get - /api/song/
export const get_all_songs = async () => {
    const sql_get = `SELECT * FROM song`;
    let [rows] = await db.query(sql_get);
    return rows;
}
// get - /api/song/user/:id
export const get_artist_song = async (artist_id:string) => {
    const sql_get = `SELECT * FROM song where artist = ${artist_id}`;
    let [rows] = await db.query(sql_get);
    return rows;
}


export const updateStatus_song = async (song_id:string,status:string) => {
    const sql_get = `SELECT * FROM statuses where status_name=?`;
    let [rows] = await db.query(sql_get,status) as any;
    if(!rows || !rows[0]){
        throw new Error("un excepted status!");
    }
    const sql_update = `UPDATE song SET status = ? WHERE id=?`;
    let [result] = await db.execute(sql_update,[status,song_id]);
    return result;
}
 // get - /api/song/:id
export const getSongById = async (song_id:string) => {
    const sql_get = `SELECT * FROM song where id = ${song_id}`;
    let [rows] = await db.query(sql_get);
    return rows;
}
export const deleteAllSongsOfArtist = async (artist_id:string) => {
    const sql_get = `DELETE FROM song WHERE artist=${artist_id}`;
    //check about song in playlist
    let [rows] = await db.query(sql_get);
    return rows;
}

// // put - /api/song/:id
export const updateSongById = async (song_id:string, payload:any) => {
    const sql_update = `UPDATE song SET ? WHERE id=?`;
    let results = await db.query(sql_update,[payload,song_id]);
    const result : RowDataPacket = results[0] as RowDataPacket;
    const ok = {status:200, message:"song update succssfuly"};
     const fail = {status:200, message:"song update failed"};
     const {status, message} = result.affectedRows? ok: fail;
    return [status , {message, result}];
}

// delete - /api/song/:id
export const deleteSongById = async (song_id:string) => {    
    //delete song in playlist
    const query = `DELETE FROM playlist_song WHERE song_id=${song_id}`;
    await db.execute(query);
    const sql_get = `DELETE FROM song WHERE id=${song_id}`;
    let [rows] = await db.query(sql_get);
    return rows;   
}
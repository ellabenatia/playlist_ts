import mysql from "mysql2/promise";
import log      from "@ajar/marker";

  export let connection : mysql.Connection;
const{
    DB_HOST="localhost",
    DB_PORT="3306",
    DB_NAME="crud_demo",
    DB_USER_NAME="root",
    DB_USER_PASSWORD="qwerty"
} = process.env;

export const connect = async () => {
     if(connection) return connection;
     connection = await mysql.createConnection({
        host: DB_HOST,
        port: Number(DB_PORT),
        database: DB_NAME,
        user: DB_USER_NAME,
        password:DB_USER_PASSWORD
    });
    await connection.connect();
    log.magenta(" ✨  Connected to Mysql DB ✨ ");
};

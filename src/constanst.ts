/* eslint-disable @typescript-eslint/no-unused-vars */
export enum ROLE {
    ADMIN,
    MODERATOR,
    USER,
    GUEST
  }

export enum STATUS {
    PENDDING,
    REJECTED,
    ACCEPTED
  }